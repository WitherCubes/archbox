#!/bin/bash

set -euo pipefail

# Variables you need to change
SYSTEM="BIOS"
ZONE="Asia"
SUBZONE="Kolkata"
HOSTNAME="rt"
USERNAME="wc"
GRUB_INSTALL_LOCATION="sda"

main_install() {
    # Some needed settings
    pacman -S --noconfirm zsh dash
    ln -sf /usr/share/zoneinfo/${ZONE}/${SUBZONE} /etc/localtime
    timedatectl set-ntp true
    hwclock --systohc
    sed -i '177s/.//' /etc/locale.gen
    locale-gen
    echo "LANG=en_US.UTF-8" >> /etc/locale.conf

    # Hostname
    echo "${HOSTNAME}" >> /etc/hostname
    echo "127.0.0.1 localhost" >> /etc/hosts
    echo "::1       localhost" >> /etc/hosts
    echo "127.0.1.1 ${HOSTNAME}.localdomain ${HOSTNAME}" >> /etc/hosts

    # Root user config
    echo "Enter Root Password: "
    read PASSWD1
    echo "Enter Root Password Again: "
    read PASSWD2
    if [[ $PASSWD1 == $PASSWD2 ]]; then
        echo root:${PASSWD1} | chpasswd
    else
        echo "YOU SHOULD HAVE DONE IT!"
    fi

    # Installing basic packages
    sed -i 's/^#Para/Para/' /etc/pacman.conf
    pacman -S --needed --noconfirm xf86-video-intel xf86-video-qxl pacman-contrib grub efibootmgr networkmanager network-manager-applet dialog wpa_supplicant git mtools dosfstools reflector base-devel linux-headers bluez bluez-utils ufw gufw rsync os-prober ntfs-3g terminus-font wget

    # GRUB install
    if [[ $SYSTEM == "UEFI" ]]; then
        grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
    else
        grub-install --target=i386-pc /dev/${GRUB_INSTALL_LOCATION}
    fi
    sed -i 's/quiet/pci=noaer/g' /etc/default/grub
    grub-mkconfig -o /boot/grub/grub.cfg

    # Some systemd services
    systemctl enable NetworkManager
    systemctl enable bluetooth
    systemctl enable reflector.timer
    systemctl enable ufw

    # Home user config
    useradd ${USERNAME} -m -g users -G wheel,storage,power,network,video,audio,lp -s /bin/zsh
    echo "Enter User Password: "
    read PASSWD3
    echo "Enter User Password Again: "
    read PASSWD4
    if [[ $PASSWD3 == $PASSWD4 ]]; then
        echo ${USERNAME}:${PASSWD3} | chpasswd
    else
        echo "YOU SHOULD HAVE DONE IT!"
    fi

    # Sudo Config
    echo "${USERNAME} ALL=(ALL) ALL" >> /etc/sudoers.d/${USERNAME}

    # Copying post install to new user
    post_install_path="/home/${USERNAME}/post_install.sh"
    sed '1,/^#Post$/d' chroot_install.sh >> $post_install_path
    chmod 755 $post_install_path
    chown wc $post_install_path
    echo "Run post_install.sh after reboot"
}

### Execution ###
main_install
exit

#Post
#!/bin/bash

set -euo pipefail

initial_stuff() {
    # Clone and create necessary directories
    mkdir -p ~/dl ~/docs ~/pix/ss ~/vids ~/tmp ~/vbox ~/git ~/.local/share ~/.local/src
    git clone https://codeberg.org/WitherCubes/dots.git ~/dots
}

chaotic_aur_config() {
    sudo pacman -Sy aria2
    sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
    sudo pacman-key --lsign-key FBA220DFC880C036
    aria2c 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst'
    aria2c 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
    sudo pacman -U 'chaotic-keyring.pkg.tar.zst'
    sudo pacman -U 'chaotic-mirrorlist.pkg.tar.zst'
    sudo cp ~/dots/arch/pacman.conf /etc/pacman.conf
    sudo mkdir -p /etc/pacman.d/hooks
    sudo cp ~/dots/arch/dashbinsh.hook /etc/pacman.d/hooks/dashbinsh.hook
    sudo cp ~/dots/arch/mirrorupgrade.hook /etc/pacman.d/hooks/mirrorupgrade.hook
}

install_pacman_packages() {
    sudo pacman -Sy
    sudo pacman -S paru
    sudo pacman -S --needed - < ~/dots/arch/pacman-pkgs.txt
    paru -S --needed - < ~/dots/arch/aur-pkgs.txt
}

install_source_packages() {
    git clone https://codeberg.org/WitherCubes/suckless.git ~/.local/src/suckless
    git clone https://codeberg.org/NRK/sxbm.git ~/.local/src/sxbm

    make -C ~/.local/src/suckless/dmenu
    make -C ~/.local/src/suckless/slock
    chmod +x ~/.local/src/sxbm/sxbm ~/.local/src/sxbm/extra/sxbm_dmenu

    sudo make -C ~/.local/src/suckless/dmenu clean install
    sudo make -C ~/.local/src/suckless/slock clean install

    sudo cp ~/.local/src/sxbm/sxbm /usr/local/bin/
    sudo cp ~/.local/src/sxbm/extra/sxbm_dmenu /usr/local/bin/
}

install_dotfiles() {
    make -C ~/dots install-dots
    sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    nvim -c ':PlugInstall'

    # UFW settings
    sudo ufw default deny incoming
    sudo ufw default allow outgoing
    sudo ufw enable

    # Enable Pipewire
    systemctl --user enable pipewire-pulse.service
}

### Execution ###
initial_stuff
chaotic_aur_config
install_pacman_packages
install_source_packages
install_dotfiles
